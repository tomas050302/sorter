﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Skrt {
    class Menu {
        public static byte Nav() {
            byte op = 1;
            ConsoleKeyInfo key;
            byte j = 3;
            Console.CursorVisible = false;

            Console.SetCursorPosition(13, j);
            Console.Write("»");

            do {
                key = Console.ReadKey(true); // the key pressed will not be showed on screen
                Console.Write("\b \b"); // clean up the last '»' on the screen
                switch (key.Key) {
                    case ConsoleKey.UpArrow:
                        j -= 2;
                        if (j >= 3) {
                            op--;
                        } else {              
                            j += 2;
                        }
                        break;
                    case ConsoleKey.DownArrow:
                        j += 2;
                        if (j <= 6) {
                            op++;
                        } else {
                            j -= 2;
                        }
                        break;
                }
                Console.SetCursorPosition(13, j);
                Console.Write("»");
            } while (key.Key != ConsoleKey.Enter);
            Console.CursorVisible = false;

            return op;
        }

        public static void DrawMainMenu() {
            Console.WriteLine("\n      <Sorter by Tomás Lopes>    11/07/2019\n");
            Console.WriteLine("               Fix Song Names\n");
            Console.WriteLine("               Add Exceptions");
        }
        
        public static void DrawExceptionWordsMenu() {
            Console.WriteLine("\n      <Sorter by Tomás Lopes>    11/07/2019\n");
            Console.WriteLine("               Add Exception Words\n");
            Console.WriteLine("               See Exception Words");
        }
    }
}
