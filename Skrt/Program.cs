﻿using System;
using System.IO;

namespace Skrt {
    static class Program {
        static void Main(string[] args) {
            Menu.DrawMainMenu();
            byte selectedOption = Menu.Nav();

            handleOption(selectedOption);
        }

        static void handleOption(byte op) {
            switch (op) {
                case 1:
                    fixSongNames();
                    break;
                case 2:
                    displayExceptionWordsMenu();
                    break;
            }
        }

        static void displayExceptionWordsMenu() {
            Menu.DrawExceptionWordsMenu();
            byte selectedOption = Menu.Nav();
            switch (selectedOption) {
                case 1:
                    addWordsToExceptionFile();
                    break;
                case 2:
                    displayExceptionWordsFile();
                    break;
            }
        }

        static void addWordsToExceptionFile() {
            // TODO: check if the file exists
            // Get exception words separated by a line
        }

        static void displayExceptionWordsFile() {
            // check if the file exists
            // get the data from the file and display it in the console
        }

        static void fixSongNames() {
            string directory = getInput();

            DirectoryInfo mainDirInfo = getMainDirectoryInfo(directory);
            DirectoryInfo[] subDirsInfo = getSubDirectoriesInfo(mainDirInfo);

            renameDirectoryFiles(mainDirInfo);
            renameEachSubDirectory(subDirsInfo);
        }

        static string getInput() {
            Console.Write("Root Directory: ");
            return Console.ReadLine();
        }

        static DirectoryInfo getMainDirectoryInfo(string directory) {
            return new DirectoryInfo(directory);
        }

        static DirectoryInfo[] getSubDirectoriesInfo(DirectoryInfo mainDir) {
            return mainDir.GetDirectories();
        }

        static void renameEachSubDirectory(DirectoryInfo[] dirsInfo) {
            foreach (DirectoryInfo directory in dirsInfo) {
                renameDirectoryFiles(directory);
            }
        }

        static void renameDirectoryFiles(DirectoryInfo dir) {
            FileInfo[] fileInfos = dir.GetFiles("*.mp3");

            foreach (FileInfo file in fileInfos) {
                renameFile(file);
            }
        }

        static void renameFile(FileInfo file) {
            string newFileName = fixName(file.Name);

            renameTo(file, newFileName);
        }

        static void renameTo(FileInfo file, string newName) {
            string oldFileFullName = file.FullName;
            string newFileFullName = file.DirectoryName + @"\" + newName;

            File.Move(oldFileFullName, newFileFullName);
        }

        static string fixName(string fileName) {
            string[] words = fileName.Split(' ');
            string[] newFileNameArray = new string[1];

            for (int i = 0; i < words.Length; i++) {
                if (wordIsNotEmpty(words[i])) {
                    string newWord = fixWord(words[i]);
                    addToArray(ref newFileNameArray, newWord);
                }
            }

            string newFileName = arrayToString(newFileNameArray);

            return newFileName;
        }

        static void addToArray(ref string[] array, string word) {
            if (array[0] != null) {
                Array.Resize(ref array, array.Length + 1);
            }

            int pos = array.Length - 1;
            array[pos] = word;
        }

        static bool wordIsNotEmpty(string word) {
            if (word == "") {
                return false;
            } else {
                return true;
            }
        }

        static string arrayToString(string[] words) {
            string stringWord = "";

            foreach (string word in words) {
                stringWord += word + " ";
            }

            return stringWord;
        }

        static string fixWord(string word) {
            string firstLetter = word.Substring(0, 1);
            string upperCaseFirstLetter = firstLetter.ToUpper();
            string wordWithFirstLetterInUpperCase = word.Replace(firstLetter, upperCaseFirstLetter);

            return wordWithFirstLetterInUpperCase;
        }
    }
}